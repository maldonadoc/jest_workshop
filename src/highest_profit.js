export const getProfit = (values) => {

  const weekPriceCar = values.split(',');
  const weekPriceCarLength = weekPriceCar.length;

  let highest_profit = 0;

  for (let i = 0; i < weekPriceCarLength; i++) {
    const pointer = weekPriceCar[i];
    for (let k = i + 1; k < weekPriceCarLength; k++) {
      let currentKvalue = weekPriceCar[k];
      let rest = (currentKvalue - pointer);
      if (isBiggerThanPointer(pointer, currentKvalue) && isBiggerThanLastHighValue(highest_profit, rest)) {
        highest_profit = rest;
      }
    }
  }

  return highest_profit;
};


const isBiggerThanPointer = (pointer, currentKvalue) => currentKvalue > pointer;

const isBiggerThanLastHighValue = (highest, rest) => highest < rest;