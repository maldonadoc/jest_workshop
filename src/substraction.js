export const substraccionDifference = (numbersArray) => {

  const r = numbersArray
    .toString()
    .split("")
    .reduce((r, current) => {
      r.multAccum *= current;
      r.sumAccum += parseInt(current);
      return r;
    }, { multAccum: 1, sumAccum: 0 });

  return r.multAccum - r.sumAccum;
}