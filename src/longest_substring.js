export const longestSubstring = (phrase) => {
  let substring = '';
  let longest = '';
  const splitted = phrase.split('');
  const length = splitted.length;

  for (let i = 0; i < length; i++) {
    if (substring.indexOf(splitted[i]) > -1) {
      substring = splitted[i];
    } else {
      substring += splitted[i];
    }

    if (substring.length > longest.length) {
      longest = substring
    }
  }

  return longest;
}