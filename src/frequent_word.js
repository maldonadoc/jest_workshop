
const removeSpaces = x => x.length > 0;

const toCleanWordsArray = (parrafo) => parrafo
  .replace(/\,|\.|\!/g, ' ')
  .split(' ')
  .filter(removeSpaces)
  .map((word) => word.toLowerCase());

const allowedWords = (words, palabrasNoPermitidas) =>
  [... new Set(words)]
    .filter((uniqueWords) => palabrasNoPermitidas.indexOf(uniqueWords) < 0);

const wordCounting = (currentAllowed, words) =>
  words
    .filter(w => w == currentAllowed)
    .length;

export const mostCommonWord = (parrafo, palabrasNoPermitidas) => {
  let biggestCount = 0;
  let wordFrequent = '';

  const words = toCleanWordsArray(parrafo);

  allowedWords(words, palabrasNoPermitidas)
    .forEach((allowedWord) => {
      let wordCounter = wordCounting(allowedWord, words);

      if (wordCounter > biggestCount) {
        wordFrequent = allowedWord;
        biggestCount = wordCounter;
      }
    });

  return wordFrequent;
}