export const withPairs = (array) => {

  const positiveIndex = getPositiveIndex(array);
  if (positiveIndex < 0) {
    return []
  }

  return [...getPairs(array, positiveIndex)].sort();
};

const getPairs = (array, positiveIndex) => {
  const pairs = new Set();
  const length = array.length;

  for (let negative = 0; negative < positiveIndex; negative++) {
    for (let positive = positiveIndex; positive <= length; positive++) {
      if (absoluteNumber(array[negative]) == array[positive]) {
        pairs.add(array[positive]);
      }
    }
  }

  return pairs;
};

const absoluteNumber = (x) => Math.abs(x)

const getPositiveIndex = (array) => array.findIndex(x => x > 0);