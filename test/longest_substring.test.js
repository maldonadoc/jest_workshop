import { longestSubstring } from '../src/longest_substring';

describe('Extract longest substring with no duplicate', () => {
  it('abcabcbb => abc', () => {
    expect(longestSubstring('abcabcbb')).toEqual('abc');
  });
});