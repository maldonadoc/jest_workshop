import { withPairs } from '../src/positive_array'

describe('positive with pair negative on array ', () => {

  it('array with pairs 1,2,3', () => {
    expect(withPairs([-3, -2, -1, -4, 3, 2, 1, 5])).toEqual([1, 2, 3])
  });

  it('array with pairs 1,2,3 and duplicate number', () => {
    expect(withPairs([-3, -2, -1, -4, 3, 3, 2, 1, 5])).toEqual([1, 2, 3])
  });

  it('array with no pairs', () => {
    expect(withPairs([-1, -2, -3])).toEqual([]);
  });

});