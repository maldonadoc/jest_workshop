import { substraccionDifference } from 'app/substraction';

describe('substraction, multiplying and sum of numbers', () => {


  it('123456789', () => {
    expect(substraccionDifference('123456789')).toBe(362835)
  });

  it('85712', () => {
    expect(substraccionDifference('85712')).toBe(537)
  });

  it('638462', () => {
    expect(substraccionDifference(638462)).toBe(6883)
  });


});