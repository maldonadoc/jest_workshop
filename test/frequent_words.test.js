import { mostCommonWord } from '../src/frequent_word';

describe('count frequent words', () => {
    it('a, a, a, a, b,b,b,c, c', () => {
        expect(mostCommonWord('a, a, a, a, b,b,b,c, c', ['a'])).toBe('b')
    });

    it("Bob hit a ball, the hit BALL flew long after it was hit.", () => {
        expect(mostCommonWord("Bob hit a ball, the hit BALL flew long after it was hit.", ["hit"]))
            .toBe("ball");
    });

});