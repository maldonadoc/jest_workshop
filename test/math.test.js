import { sum, substract } from 'app/math';

describe('math file test', () => {

    it('should to sum two values', () => {
        expect(sum(1, 3)).toBe(4)
    });

    it('should substract two values', () => {
        expect(substract(10, 9)).toBe(1);
    });
});