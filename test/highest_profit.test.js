import { getProfit } from '../src/highest_profit'

describe('highest profit', () => {

  it('with profit', () => {
    expect(getProfit('7,1,5,3,6,4')).toEqual(5);
  });

  it('no profit', () => {
    expect(getProfit('7,6,4,3,1')).toEqual(0);
  });

  it('highest on rest value', () => {
    expect(getProfit('2,8,1,5,3,2')).toEqual(6);
  })
});